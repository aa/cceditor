// avoid mixing import style with module.exports
// import CodeMirror from 'codemirror';

function parse_fragment (url) {
    // console.log("parse_fragment", url);
    ret = {}
    if (url.indexOf("#") >= 0) {
        var p = url.split("#", 2);
        ret.base = p[0];
        ret.fragment = p[1];
    } else {
        ret.base = url;
        ret.fragment = '';
    }
    return ret;
}

var CodeMirror = require("codemirror"),
    timecode = require("./timecode.js"),
    CCFrame = require("./ccframe.js");

require('codemirror/mode/markdown/markdown');
require('codemirror/lib/codemirror.js');
require('codemirror/addon/mode/overlay.js');
require('codemirror/mode/markdown/markdown.js');
require('codemirror/addon/fold/foldcode.js');
require('codemirror/addon/fold/foldgutter.js');
require('codemirror/addon/fold/markdown-fold.js')
require("codemirror/theme/monokai.css")

require("codemirror/lib/codemirror.css");
require("codemirror/addon/fold/foldgutter.css");
require("./editor.css");

function getText (url, callback) {
    var request = new XMLHttpRequest();
    // request.open('GET', url, true);
    request.open('GET', url + "?v="+Math.random(), true);

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        // Success!
        callback(null, request.responseText);
      } else {
        callback("server error", null);
      }
    };

    request.onerror = function() {
        callback("connection error", null);
    };
    request.send();
}

function post (url, data, callback) {
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    // request.setRequestHeader('Content-Type', 'application/octet-stream');
    request.onload = function() {
      if (request.readyState == XMLHttpRequest.DONE && request.status >= 200 && request.status < 400) {
        // Success!
        var resp = request.responseText;
        callback(null, resp);
      } else {
        callback("server error");
      }
    };
    request.onerror = function() {
        callback("connection error");
    };
    var sdata = "";
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            sdata += (sdata?"&":"") + key+"="+encodeURIComponent(data[key]);
        }
    }
    request.send(sdata);            
}
/*
// Not using...
// Define a codemirror "overlay" mode -- extend markdown to include timecodes

CodeMirror.defineMode("ccmarkdown", function(config, parserConfig) {
  var ccMarkdownOverlay = {
    token: function(stream, state) {
      // var ch;
      if (stream.match(tcpat)) {
          return "cctimecode";
      }
      while (stream.next() != null && !stream.match(tcpat, false)) {}
      return null;
    }
  };
  return CodeMirror.overlayMode(CodeMirror.getMode(config, parserConfig.backdrop || "text/x-markdown"), ccMarkdownOverlay);
});
*/

function strip_fragment (url) {
    var p = url.indexOf("#");
    if (p >= 0) { return url.substring(0, p); }
    return url;
}

// var sample_text = "[0029#t=00:27:08.216]\nThen we get to the Critique of the political practice of Detournement. \n\n[0029]: /video/2017-12-13/MVI_0029.web.mp4 \"video\"\n[0030]: /video/2017-12-13/MVI_0030.web.mp4 \"video\"\n\n* <https://en.wikipedia.org/wiki/Gruppe_SPUR>\n* <http://www.notbored.org/spur.html>\n[scans#7/-7.289/231.754]\n\n[scans]: /st01.scans.html \"scans\"\n\n";

class CCEditor {
    constructor (elt, opts) {
        this.opts = opts || {};
        this.elt = elt;
        // this.elt.classList.add("cceditor");
        this.editorelt = document.createElement("div");
        this.editorelt.classList.add("cceditor");
        // this.textarea = document.createElement("textarea");
        // this.textarea.classList.add("cceditor_textarea");
        // this.textarea.value = sample_text;
        // this.editorelt.appendChild(this.textarea);
        this.elt.appendChild(this.editorelt);
        // this.editor = CodeMirror.fromTextArea(this.textarea, {
        this.editor = CodeMirror(this.editorelt, {
            // mode:  "ccmarkdown",
            mode:  "text/x-markdown",
            lineNumbers: true,
            lineWrapping: true,
            foldGutter: true,
            value: "",
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        });
        this.current_href = undefined;
        this.players_by_target = {};
        this.default_player = undefined;
        this.document = document;
        this.window = window;
        this.elt.addEventListener("click", this.clicker.bind(this));
        this.elt.addEventListener("touchend", this.clicker.bind(this));
        // window.addEventListener("hashchange", this.hashchange.bind(this));
        // this.hashchange();
        this.setup_keys();
    }
    /* Map URL and timecode clicks */
    clicker (e) {
        if (this.opts.click) {
            this.opts.click.call(this, e.target);
        }        
    }

    hashchange () {
        var h = window.location.hash;
        if (h) {
            var m = h.match(/^#(.+)/);
            if (m !== null) {
                var href = m[1];
                if (href !== current_href) {
                    current_href = href;
                    getText(href, function (err, text) {
                        if (err) {
                            console.log("cceditor.hashchange: ERROR", err);
                            return;
                        }
                        editor.setValue(text);
                    })
                }
            }
        }
    }
    display_link (href, target) {
        target = target || "";
        if (this.opts.click) {
            this.opts.click.call(this, href, target);
        }
        // this.player_for_target(target).src = href;
    }
    make_player_cell () {
        var pelt = document.createElement("div");
        pelt.classList.add("cceditor_cell");
        this.elt.appendChild(pelt);
        return new CCFrame(pelt);
    }
    player_for_target (t) {
        if (t) {
            var p = this.players_by_target[t];
            if (p === undefined) {
                p = this.make_player_cell();
                this.players_by_target[t] = p;
            }        
        } else {
            var default_player_elt = document.getElementById("cceditor_default_player");
            if (!default_player_elt) {
                p = this.default_player = this.make_player_cell();
                p.elt.id = "cceditor_default_player"; 
            } else {
                return this.default_player;
            }
        }
        // console.log("player_for_target", t, p);
        return p;
    }
    do_save () {
        var that = this;
        var e = this.editor;
        // console.log("cceditor: do_save", e, this.current_href);
        var text = e.getValue();
        // var savebutton = gebi("save");
        // savebutton.classList.add("saving");
        // savebutton.disabled = true;
        // status.innerHTML = "saving...";
        if (this.current_href) {
            if (this.opts.saver) {
                post(this.opts.saver, {path: this.current_href, text: text}, function (err, resp) {
                    if (err) {
                        console.log("cceditor: save ERROR", err);
                        if (that.opts.notie) {
                            that.opts.notie.alert({text: "Error saving ("+err+")", type: "error"})
                        }
                    } else {
                        resp = JSON.parse(resp);
                        if (resp.error) {
                            console.log("cceditor: save ERROR", resp.message);
                            if (that.opts.notie) {
                                that.opts.notie.alert({text: "Error saving ("+resp.message+")", type: "error"})
                            }
                        } else {
                            // console.log("cceditor: saved", resp);
                            if (that.opts.notie) {
                                that.opts.notie.alert({text: "Saved", type: "success"})
                            }                            
                        }
                        // console.log("cceditor: post", resp);
                        // status.innerHTML = "Saved " + new Date();
                    }

                });
            } else {
                post(this.current_href, {text: text}, function (err, resp) {
                    // savebutton.disabled = false;
                    // savebutton.classList.remove("saving");
                    if (err) {
                        console.log("cceditor: post ERROR", err);
                        if (that.opts.notie) {
                            that.opts.notie.alert({text: "Error saving", type: "error"})
                        }
                    } else {
                        // console.log("cceditor: saved");
                        if (that.opts.notie) {
                            that.opts.notie.alert({text: "Saved", type: "success"})
                        }
                        // console.log("cceditor: post", resp);
                        // status.innerHTML = "Saved " + new Date();
                    }
                })

            }

        }
    }
    setup_keys () {
        // console.log("cceditor: setup_keys");
        var that = this;
        this.editor.setOption("extraKeys", {
            "Ctrl-Down": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "down"); }
            },
            "Alt-Down": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "down"); }
            },
            "Ctrl-Up": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "up"); }
            },
            "Alt-Up": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "up"); }
            },
            "Ctrl-Left": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "left"); }
            },
            "Alt-Left": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "left"); }
            },
            "Ctrl-Right": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "right"); }
            },
            "Alt-Right": function(cm) {
                if (that.opts.ctrlKey) { that.opts.ctrlKey.call(that, "right"); }
            },
            "Ctrl-S": function (cm) {
                that.do_save();
            }
        });        
    }
    set src (href) {
        // console.log("cceditor.set src", href);
        if (href && href !== this.current_href) {
            this.current_href = href;
            var that = this;
            getText(href, function (err, text) {
                if (err) {
                    console.log("cceditor: ERROR", err);
                    return;
                }
                // console.log("gottext", text, this.editor);
                that.editor.setValue(text);
            })
        }
    }
    get src () {
        return this.current_href;
    }
    getValue () {
        return this.editor.getValue();
    }
    getSelection () {
        return this.editor.getSelection();
    }
    replaceSelection (text) {
        this.editor.replaceSelection(text);
    }
    resize () {
        // console.log("cceditor.resize", CodeMirror, this);
        this.editor.refresh();
        // CodeMirror.resize();
    }
}

module.exports = {
    CodeMirror: CodeMirror,
    CCEditor: CCEditor,
    parse_fragment: parse_fragment
}