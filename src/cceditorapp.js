var GoldenLayout = require("golden-layout"),
    cceditor = require("./cceditor.js"),
    CCFrame = require("./ccframe.js"),
    $ = require("jquery"),
    reflinks = require("./reflinks.js"),
    notie = require("notie");

require("golden-layout/src/css/goldenlayout-base.css");
require("golden-layout/src/css/goldenlayout-light-theme.css");
require("notie/dist/notie.css");

// var DEFAULT_CONFIG = {
//     content: [{
//         type: 'row',
//         content: [{
//             type: 'component',
//             componentName: 'cceditor',
//             componentState: { href: '/docs/st01.md', target: "editor" }
//         }]
//     }]
// };

var DEFAULT_CONFIG = {
    content: [{
        type: 'row',
        content: [{
            type: 'stack',
            id: 'editorstack',
            content: [{
                type: 'component',
                componentName: 'cceditor',
                componentState: { href: '', target: "editor" }
            }]
        }]
    }]
};

// app.layout.root.contentItems
// use container.extendState/ setState to add hash to state
// Getting the state
//var state = JSON.stringify( myLayout.toConfig() );

function enclosing_stack(c) {
    while (c.type !== "stack" && c.parent) { c = c.parent; }
    if (c.type == "stack") return c;
}

class CCEditorApp {
    constructor (opts) {
        var that = this;
        if (opts === undefined) { opts = {} };
        var config = opts.layout || DEFAULT_CONFIG;
        var frames_by_target = {};
        this.frames_by_target = frames_by_target;
        this.layout = new GoldenLayout( config, opts.elt );
        this.active_stack = null;

        // Create the link control
        this.layout.on( 'stackCreated', function( stack ){
            var li = document.createElement("li");
  
            li.classList.add("hash");
            li.style.width = "auto";

            var focus = document.createElement("input");
            focus.setAttribute("type", "radio");
            focus.setAttribute("name", "cceditorlinkfocus");
            li.appendChild(focus);

            focus.addEventListener("input", function () {
                console.log("input", this, this.checked);
                if (this.checked) {
                    that.set_active_stack(stack, li);
                }
            });

            var field = document.createElement("span"),
                field_a = document.createElement("a");

            li.appendChild(field);
            field.appendChild(field_a);
            field.classList.add("value");
            field.style.position = "relative";
            field.style.top = "-3px";
            field_a.textContent = "#";
            field_a.href= "#";
            field_a.style.color = "white";
            field_a.style.textDecoration = "none";
            field_a.title = "paste this link";

            field_a.style.fontFamily = "monospace";
            field_a.style.fontSize = "14px";
            field_a.style.padding = "0 10px 0px 10px";
            field_a.style.background = "black";
            field_a.style.color = "white";

            field.addEventListener("click", function () {
                var myState = stack.getActiveContentItem().container.getState();
                // console.log("paste link", stack, myState);
                $(li).trigger("pastelink", myState);
            });

            stack.header.controlsContainer.prepend(li);
                // stack.on( 'activeContentItemChanged', function( contentItem ) {
                // console.log("ACTIVECONTENTITEMCHANGED", contentItem);
                // setColor( contentItem.container.getState().color );
                // that.set_active_stack(stack);
            // });
        });

        function cm_markdown_link_element_of_interest(elt) {
            // given a clicked element, return the "interesting" element if considered a markdown link
            // In a nutshell, always isolate the cm-url except in the case of an implicit reference link (e.g. See [my website][]. )
            if (elt.classList.contains("cm-link")) {
                // check if followed by cm-url & use that (unless blank)
                if (elt.nextSibling && 
                    elt.nextSibling.classList &&
                    elt.nextSibling.classList.contains("cm-url") &&
                    elt.nextSibling.textContent != "[]" ) {
                    return elt.nextSibling;    
                }
                return elt;
            } else if (elt.classList.contains("cm-url")) {
                if (elt.textContent == "[]" &&
                    elt.previousSibling &&
                    elt.previousSibling.classList &&
                    elt.previousSibling.classList.contains("cm-link")) {
                    return elt.previousSibling;
                }
                return elt;
            }
        }

        this.layout.registerComponent( 'cceditor', function( container, componentState ){
            // not sure why the delay is necessary but codemirror glitches (gutter layout)
            // when immediately instantiated... so a little push back...
            window.setTimeout(function () {
                var editor = new cceditor.CCEditor(container.getElement()[0], {
                        saver: opts.saver,
                        click: function (elt) {
                            var href, target;
                            // (src, classList)
                            // console.log("click", elt);
                            elt = cm_markdown_link_element_of_interest(elt);
                            if (elt) {
                                var ref = elt.textContent,
                                    m = ref.match(/\[(.+?)\]\:?/);
                                if (m) {
                                    ref = m[1].toLowerCase();
                                    // console.log("reflink", ref);
                                    var ev = this.editor.getValue();
                                    var thereflinks = reflinks.extract_reflink_definitions(ev);
                                    var reflink = reflinks.xpandRefLink(ref, thereflinks);
                                    if (reflink) {
                                        href = reflink.href;
                                        target = reflink.title;
                                    } else {
                                        console.log("warning: undefined reference in link", ref);
                                    }
                                } else {
                                    // trim ( ) and < > forms
                                    ref = ref.replace(/^\((.+)\)$/, "$1");
                                    ref = ref.replace(/^\<(.+)\>$/, "$1");
                                    // console.log("link", ref);
                                    m = ref.match(/^(.+?)(?: *(?:\"(.+)\")?)$/);
                                    href = m[1];
                                    target = m[2] || "";
                                }
                            }
                            if (href) {
                                handle_link(href, target);
                            }
                        },
                        ctrlKey : function (dir) {
                            // console.log("editor ctrlUp");
                            if (that.active_stack) {
                                var c = that.active_stack.getActiveContentItem();
                                var state = c.container.getState();
                                c = c.container.getElement();
                                // console.log("state", state);
                                if (dir == "up") {
                                    c.trigger("control", "toggle");    
                                } else if (dir == "left") {
                                    c.trigger("control", "jumpback");
                                } else if (dir == "right") {
                                    c.trigger("control", "jumpforward");
                                } else if (dir == "down") {
                                    paste_link(state);
                                }
                            }                        
                        },
                        notie: notie
                    });
                
                var initial_href = componentState.href;
                if (!initial_href) {
                    var initial_hash = window.location.hash;
                    var m = initial_hash.match(/^#(.+)/);
                    if (m !== null) {
                        initial_href = m[1];
                    }
                }

                container.setTitle(initial_href);
                frames_by_target[componentState.target] = editor;
                container.on("resize", function () {
                    // console.log("lm.resize");
                    editor.resize();
                })
                editor.src = initial_href;

                /* HASH HANDLING */
                window.addEventListener("hashchange", function (e) {
                    // console.log('hashchange', e);
                    var hash = window.location.hash,
                        m = hash.match(/^#(.+)/);
                    if (m !== null) {
                        hash = m[1];
                        // console.log("new fragment", hash);
                        open_in_editor(hash);
                    }
                }, false);

                function open_in_editor (href) {
                    var component = that.find_editor(href);
                    if (component) {
                        // console.log("found matching cceditor component", component);
                        component.parent.setActiveContentItem(component);
                    } else {
                        // var editstack = app.layout.root.getItemsById("editorstack")[0];
                        var editstack = that.get_editor_stack();
                        editstack.addChild({
                            'type': 'component',
                            componentName: 'cceditor',
                            componentState: { href: href, target: "editor" }
                        })
                    }
                }

                function handle_link (href, target) {
                    // HANDLE HREF + TARGET
                    // console.log("handle_link", href, target);
                    if (href.match(/\.md$/) && !target)  {
                        // EDITOR LINK
                        // console.log("editor link");
                        open_in_editor(href);
                    } else {
                        // OTHER LINKS
                        target = target || "link";

                        var linksstack = that.layout.root.getItemsById("linkstack");
                        if (linksstack.length > 0) {
                            linksstack = linksstack[0];
                            // console.log("got linksstack", linksstack);
                            // look for matching target
                            var t = that.find_frame_by_target(target);
                            if (t) {
                                var f = frames_by_target[target];
                                if (f) {
                                    // console.log("cceditorapp: using ccframe", f);
                                    f.src = href;
                                    // maybe only if necessary (now grabs focus?!)
                                    t.parent.setActiveContentItem(t);
                                } else {
                                    console.log("cceditorapp: no ccframe in frames_by_target for target", target);
                                }
                            } else {
                                linksstack.addChild({
                                    type: 'component',
                                    componentName: 'ccframe',
                                    componentState: { href: href, target: target}
                                });
                            }
                            // if 
                        } else {
                            // Create the linkstack with a ccframe targeting this href
                            var root_row = that.layout.root.contentItems[0];
                            root_row.addChild({
                                type: 'column',
                                content: [{
                                    type: 'stack',
                                    id: 'linkstack',
                                    content: [{
                                        type: 'component',
                                        componentName: 'ccframe',
                                        componentState: { href: href, target: target}
                                    }]
                                }]
                            });
                        }
                        // console.log("click", href, target);
                        // var p = frames_by_target[target]; 
                        // if (p) { p.src = href; }                            
                    }

                }

                function paste_link (state) {
                    var ev = editor.getValue();
                    // console.log("paste_link");
                    var thereflinks = reflinks.extract_reflink_definitions(ev);
                    var href = state.href;
                    if (state.hash) {
                        href = cceditor.parse_fragment(state.href).base + state.hash  
                    }
                    // var curtext = editor.getSelection();
                    // console.log("curtext", curtext, curtext.search(/\n/));
                    href = reflinks.compactRefLink(href, thereflinks);
                    editor.replaceSelection("["+href+"]");                    
                }

                // Listen for pastelink events
                $(document).on("pastelink", function (e, data) {
                    // console.log("editor caught pastelink", e, data);
                    paste_link(data);
                })

            }, 10);
        });
        this.layout.registerComponent( 'testComponent', function( container, componentState ){
            container.getElement().html( '<h2>' + componentState.label + '</h2>' );
        });
        this.layout.registerComponent( 'ccframe', function( container, componentState ){
            var elt = container.getElement()[0],
                player = new CCFrame(elt);
            elt.classList.add("ccframe");
            $(elt).on("hashchange", function (e, h) {
                // console.log("index:ccframe hashchange", e, h);
                // watch ccframe element for hashchange (synthetic) events
                // and update the container (component) state with the new hash
                container.extendState({ hash: h });
                var es = enclosing_stack(container);
                if (es) {
                    var hc = es.header.controlsContainer.find(".hash .value a");
                    if (hc) {
                        hc[0].textContent = h; // h.substring(1); // strip leading hash symbol
                    }
                }
            }).on("srcchange", function (e, url) {
                // console.log("index.ccframe srcchange", url);
                container.extendState({ href: url });                
            })
            // console.log("ccframe.init", componentState);
            var use_href = componentState.href;
            if (componentState.hash) {
                use_href = cceditor.parse_fragment(componentState.href).base + componentState.hash;
            }
            player.src = use_href;
            container.setTitle(componentState.target);
            frames_by_target[componentState.target] = player;
            // window.setTimeout(function () {
            //     console.log("editor", editor);
            //     editor.editor.refresh();
            // }, 250)
        });
        this.layout.init();
    }
    set_active_stack(stack, li) {
        // console.log("set_active_stack", li, this);
        if (this.active_stack != stack) {
            if (this.active_stack) {
                var hash = this.active_stack.header.controlsContainer.find(".hash");
                hash[0].style.opacity = 0.4;
            }
            this.active_stack = stack;
            if (this.active_stack) {
                var hash = this.active_stack.header.controlsContainer.find(".hash");
                hash[0].style.opacity = 1.0;
                var radio = this.active_stack.header.controlsContainer.find(".hash input");
                console.log("radio", radio);
                radio[0].checked = true;
            }            
        }
    }
    get_editor_stack (n) {
        if (n === undefined) { n = this.layout.root };
        if (n.type === "component" && n.componentName == "cceditor") {
            if (n.parent && n.parent.type == "stack") {
                return n.parent;
            }
        }
        for (var i=0, l=n.contentItems.length; i<l; i++) {
            var result = this.get_editor_stack(n.contentItems[i]);
            if (result) { return result; }
        }
    }

    find_editor (href, n) {
        if (n === undefined) { n = this.layout.root };
        if (n.type === "component" && n.componentName == "cceditor") {
            if (n.config.componentState.href == href) { return n; }
        }
        for (var i=0, l=n.contentItems.length; i<l; i++) {
            var result = this.find_editor(href, n.contentItems[i]);
            if (result) { return result; }
        }
    }

    find_frame_by_target (target, n) {
        if (n === undefined) { n = this.layout.root };
        if (n.type === "component" && n.componentName == "ccframe") {
            if (n.config.componentState.target == target) { return n; }
        }
        for (var i=0, l=n.contentItems.length; i<l; i++) {
            var result = this.find_frame_by_target(target, n.contentItems[i]);
            if (result) { return result; }
        }
    }
}

module.exports = {
    CCEditorApp: CCEditorApp,
    CodeMirror: cceditor.CodeMirror,
    CCEditor: cceditor.CCEditor,
    gl: GoldenLayout,
    notie: notie
}